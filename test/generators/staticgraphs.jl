@testset "Staticgraphs" begin
	@testset "tree" begin
		s₁ = [7, 1, 3, 7, 10, 1, 8, 10, 10]
		s₂ = [4, 4, 4, 5]
		s₃ = [1, 1]
		s₄ = [i for i in 2:100]

		T₁ = tree(s₁)
		T₂ = tree(s₂)
		T₃ = tree(s₃...)
		T₄ = tree(s₄...)

		# Testing for self-edges
		@test !has_self_loops(T₁)
		@test !has_self_loops(T₂)
		@test !has_self_loops(T₃)
		@test !has_self_loops(T₄)

		# TODO: Test that the graphs are trees

		# Test that the graphs correspond to the Prüfer sequence
		@test T₃ == star_graph(4)
		@test T₄ == path_graph(101)
	end
end