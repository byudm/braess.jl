using LightGraphs
using BraessParadox
using LinearAlgebra
using ResistanceDistance
using Test

const testdir = dirname(@__FILE__)

tests = [
        "generators/staticgraphs"
]

@testset "BraessParadox" begin
	for t in tests
		path = joinpath(testdir, "$(t).jl")
		include(path)
	end
end
