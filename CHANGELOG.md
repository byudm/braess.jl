# Release Notes

## BraessParadox v0.1.3
### Added
- Test suites and CI/CD

### Fixed
- Logic error in tree generator causing the graph to contain self-loops
- Syntax error in tree dispatcher

## BraessParadox v0.1.2
### Fixed
- Logic error in barbell generator causing the path to be the wrong size
- Logic error in braess closure preventing the function from adding all brass edges.
- Matched argument names and docstrings

## BraessParadox v0.1.1
### Fixed
- Various syntax errors causing module to fail precompilation
- Logic error in `braess_closure!` causing the function to perform erratically

## BraessParadox v0.1.0
### Added
- Relevant graph operators not defined by `LightGraphs.jl`, especially graph separations and sums
- Generators for current graphs of interest
- Functions for computing effective resistance and Kemeny's constant
- Functions for identifying and adding Braess edges to graphs
