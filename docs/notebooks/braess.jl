### A Pluto.jl notebook ###
# v0.12.20

using Markdown
using InteractiveUtils

# ╔═╡ e6dbe4fa-1f8d-11eb-312b-dfe9115f9610
let
	import Pkg
	
	Pkg.activate(mktempdir())
	Pkg.add(["GraphPlot", "Colors"])
	Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/byudm/ResistanceDistance.jl"))
	Pkg.add(Pkg.PackageSpec(url="https://gitlab.com/byudm/BraessParadox.jl"))
	
	using LightGraphs, GraphPlot, Colors, BraessParadox, ResistanceDistance
end

# ╔═╡ d2c3260c-65a3-11eb-140a-398e9e95a007
md"_research notes on Braess' Paradox_"

# ╔═╡ 213b0160-1473-11eb-374f-2f7e586c2244
md"""
# Braess' Paradox
For a finite simple connected graph ``G,`` the random walk on the vertices of ``G`` is
an ergodic Markov chain. Hence, the _Kemeny's constant_ of the Markov chain is
defined and equivalent to
```math
	\mathcal K(G) = \frac{d^T R d}{4m} = \frac{1}{4m} \sum_{i, j \in G} d_i d_j 
	r_G(i,j),
```
where ``R`` is the resistance matrix of ``G,`` ``m`` is the number of edges of the
graph, and ``d_i`` is the degree of ``i \in V(G),`` and ``d`` is the vector whose
``i``-th entry is ``d_i.``

The Kemeny's constant of a random walk is a measure of the expected number of steps to
transition from state ``i \to j.`` As such, one would expect that adding an edge
``i \sim j \notin E(G)`` to the graph would result in a decrease of mean first passage
time, but this is not always the case. A non-edge that increases commute time when
added to a graph is referred to as a _Braess edge_ of that graph.
"""

# ╔═╡ 59d3e598-1476-11eb-2a71-7767b573c341
md"""
## Known Occurrences of Braess Edges
Recent results have proven that twin pendant vertices always induce a Braess edge. 
With our knowledge of one-separations, this generalizes easily to include 

Some examples of graphs that we have seen induce Braess edges are conjoined cycles,
the one-sum of a cycle ``C_n`` joined to any cycle ``C_m``, where ``m, n \geq 3``
and one of ``m`` or ``n`` is greater than ``3``.
"""

# ╔═╡ 4a02df10-1474-11eb-32af-2fd0b1880efb
md"""
## 1-Separations and Braess Edges
We have based our computations on resistance networks, which has led us to the 
following results.
"""

# ╔═╡ 04a69dc0-6635-11eb-270b-032759013ae1
md"""
!!! theorem "Corollary"
    Let ``G`` be a simple connected graph that is the one sum of ``G_1`` and ``G_2``
    with one-separator ``v``. We denote the number of edges in each graph as ``m_1``
    and ``m_2``, respectively, and the set of edges as ``E_1`` and ``E_1``. The set 
    ``\mathcal L = \{i \sim j: i \sim j \notin E_2\}`` with size ``l`` is Braess in
    ``G`` if
    ```math
        A m_1^2 + ((A + C)m_2 + Bl)m_1) + C(m_2^2 + lm_2) > 0,
    ```
    where ``A = \mu_{G_2'}(v) - \mu_{G_2}(v)``, ``B = \mathcal K(G_2') - \mu_{G_2}
    (v)``, and ``C = \mathcal K(G_2') - \mathcal K(G_2)``.
"""

# ╔═╡ 3fd7d458-6636-11eb-0748-e33566f3207a
md"
This result is a corollary to the main result (so far), which is itself a weighty
formula that gives an exact value for ``\mathcal K(G') - \mathcal K(G)``. Theis result
is a sufficient but not necessary condition for the ocurrence of a Braess edge.
Currently, I wish to find a more strict inequality that is a necessary condition for
a Braess edge to occur.
"

# ╔═╡ 1089d8ce-4c08-11eb-0b4b-9ba0ee64f021
md"
## Braess Sets
Although a graph may not have any Braess edges, we call a set of non-edges ``\mathcal
E`` a _Braess set_ if the graph received by adding ``\mathcal E`` has a higher
Kemeny's constant than the base graph. A Braess set is non-trivial if no subset is
Braess and the set contains at least 2 edges.

  - Find examples of graphs with Braess sets
  - Find an example of a graph with no Braess edges that has a Braess set.

By brute force I have determined that no graph of order less than ``5`` has a non-
trivial Braess set.

_Proposition._ If ``G`` is a graph with a non-trivial Braess set ``\{e_1, e_2\}``,
then neither ``G \cup e_1`` nor ``G \cup e_2`` induce a twin pendant vertex pair
not present in ``G``.
"

# ╔═╡ 073577da-358b-11eb-31bb-81041a37e7e5
md"
## Joining Graphs
What is the most general thing we can say about attaching two graphs in terms of 
Braess edges?

If pendant vertices are attached, all Braess edges are preserved

Conditions for removal/addition of edges
- When can we guarantee that Braess edges are preserved?
- When can we guarantee that no Braess edges are induced?

When does a one-sum
  - Induce a new Braess edge?
  - Preserve an existing Braess edge?
  - Eliminate an edge that was previously Braess?

When a one-separation is present
- How large does the other component need to be to induce a Braess edge in a
  - Star graph (``k`` pendant vertices)?
  - Cycle graph?
- If ``G = G_1 \oplus_v G_2``, what is the minimum ``m_1 = |E_1|`` such that every
  non-edge of ``G_2`` forms a Braess set?
- Relationship to hitting time and one-sparators
"

# ╔═╡ 49ec33e8-5a7f-11eb-1435-e5193b578d4d
md"
### Joining Graphs to Cycles
Given ``m \geq 4``, what is the minimum natural number ``n`` such that the one-sum of
any order-``n`` graph ``G`` with ``C_m`` has a Braess edge or set? We had previously 
conjectured that if ``G`` had a one-separator ``v``, there could
not exist a Braess edge of the form ``(v, j)``. We have since discovered a 
counterexample, and there's indication that if one component of the separation is
very large and well-connected, the other graph always converts into a ``k``-cluster.
"

# ╔═╡ c85b811a-6773-11eb-2b64-c5a25461028e
md"
### Moment
It seems that the _moment_ of ``v \in V(G)``, given by the expression ``\mu(G, v) =
d' R e_v``, is closely related to the Kemeny's constant and finding Braess edges.
"

# ╔═╡ fc4c3620-5a91-11eb-18e5-3dac5a1951b4
function moment(G::AbstractGraph{T}) where T <: Integer
	d = degree(G)
	R = resistance_matrix(G)
	
	(d' * R)'
end

# ╔═╡ 8b64709a-54fa-11eb-046c-75edb8b80205
md"
## Barbell Graphs
Help simplify the barbell graph equation in the overleaf. Add the results on barbells
to the beginning of the main paper with an explanation of how the one-separation
allows computation of the kemeny's constant through resistance distance.

Show that barbells maximize kconst or that creating communities farther from the
one-separation increases Kemeny's constant.

We have already obtained a counterexample to the proposition that ``B(1, n/3, n/3, 
n/3)`` is the graph that maximizes the Kemeny's constant for all graphs of order 
``n``.

Try to derive a closed-form expression for the kconst of a Barbell with ``k`` paths,
all meeting at the same vertex, join two complete graphs. An example of such a barbell
is shown below.
"

# ╔═╡ 3559658a-675a-11eb-05db-813f23c262cc
let
	G = cycle_graph(12)
	attach!(G, 12, complete_graph(5), 1)
	attach!(G, 6, complete_graph(5), 1)
	attach!(G, 6, path_graph(6), 1)
	associate!(G, 25, 12)
	
	gplot(G, nodelabel=1:nv(G), nodefillc=colorant"green")
end

# ╔═╡ 12e08f2e-6859-11eb-0a85-f7097fa53536
md"
## Conjoined Paths
Conjecture: If we take the one sum of two paths, the ends of all paths will form
``k`` or ``k-1`` clusters for some ``k \leq n/6``
"

# ╔═╡ 420ecd64-6cab-11eb-1172-898fee280706
md"
## Barbells don't maximize Kemeny's Constant
The following cells generate and automatically save counterexamples in a file titled
'barbell`k`_ce.lg'. Examples are generated by taking the Braess closure of random
tree graphs.
"

# ╔═╡ c7f4ca96-6b25-11eb-37a8-09414e1f9238
k = 7

# ╔═╡ fe07cae2-6b26-11eb-29a8-9f753b9bbdbb
G = rand_tree(3k-1)

# ╔═╡ cbd7a334-6b27-11eb-3270-df84173306de
H = braess_closure(G)

# ╔═╡ 4aa85846-6c9c-11eb-38be-0d5975cd5a23
gplot(H, nodelabel=1:3k-1, nodefillc=colorant"green")

# ╔═╡ ecc0df20-6b27-11eb-0d21-493efd9d7511
ℬ = kconst(barbell(k,k))

# ╔═╡ c63f12f0-6ca2-11eb-2569-45ef4e544228
counterexamples = if isfile("barbell$(k)_ce.lg")
	loadgraphs("barbell$(k)_ce.lg")
else
	Dict{String, AbstractGraph}()
end

# ╔═╡ f9b17166-6c9b-11eb-3def-7534e8d77bde
let
	𝒦 = kconst(H)
	
	if 𝒦 - ℬ > 0
		counterexamples["counterexample$(counterexamples.count + 1)"] = H
		savegraph("barbell$(k)_ce.lg", counterexamples)
	end
	
	md"
\\(\\mathcal K(H) = $(round(𝒦))\\)

\\(\\mathcal K(H) - \\mathcal K(B(1, k, k, k)) = $(𝒦 - ℬ)\\)
"
end

# ╔═╡ Cell order:
# ╟─e6dbe4fa-1f8d-11eb-312b-dfe9115f9610
# ╟─d2c3260c-65a3-11eb-140a-398e9e95a007
# ╟─213b0160-1473-11eb-374f-2f7e586c2244
# ╟─59d3e598-1476-11eb-2a71-7767b573c341
# ╟─4a02df10-1474-11eb-32af-2fd0b1880efb
# ╟─04a69dc0-6635-11eb-270b-032759013ae1
# ╟─3fd7d458-6636-11eb-0748-e33566f3207a
# ╟─1089d8ce-4c08-11eb-0b4b-9ba0ee64f021
# ╟─073577da-358b-11eb-31bb-81041a37e7e5
# ╟─49ec33e8-5a7f-11eb-1435-e5193b578d4d
# ╟─c85b811a-6773-11eb-2b64-c5a25461028e
# ╟─fc4c3620-5a91-11eb-18e5-3dac5a1951b4
# ╟─8b64709a-54fa-11eb-046c-75edb8b80205
# ╟─3559658a-675a-11eb-05db-813f23c262cc
# ╟─12e08f2e-6859-11eb-0a85-f7097fa53536
# ╟─420ecd64-6cab-11eb-1172-898fee280706
# ╠═c7f4ca96-6b25-11eb-37a8-09414e1f9238
# ╠═fe07cae2-6b26-11eb-29a8-9f753b9bbdbb
# ╟─cbd7a334-6b27-11eb-3270-df84173306de
# ╟─4aa85846-6c9c-11eb-38be-0d5975cd5a23
# ╟─f9b17166-6c9b-11eb-3def-7534e8d77bde
# ╟─ecc0df20-6b27-11eb-0d21-493efd9d7511
# ╟─c63f12f0-6ca2-11eb-2569-45ef4e544228
