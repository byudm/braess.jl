export separate, partition, associate, associate!, attach, attach!

"""
    separate(G, v...)

Separates `G` into two components using `v` as a list of separators.
If no separation is possible, returns two empty graphs.

# Note
Partitioning and separating a graph are two distinct operations; a partition
on a graph will inevitably result in a number of some edges being lost from a
graph, referred to as the "cut edges." On the other hand, a graph separation
includes every edge in the graph by definition.

# Examples
```jldoctest
julia> using BraessParadox;

julia> G = conjoind_cycle(4, 5)
{8, 9} undirected simple Int64 graph

julia> separate(G, 4)
({4, 4} undirected simple Int64 graph, {5, 5} undirected simple Int64 graph)
```
"""
function separate(G::SimpleGraph{T}, v::T...) where T <: Integer
    !allunique(v) && throw(ArgumentError("vertex separators must be unique"))
    len = size(v, 1)
    G₁  = SimpleGraph{T}(len)
    G₂  = SimpleGraph{T}(len)
    V₁  = zeros(T, nv(G))
    V₂  = zeros(T, nv(G))

    for (i, u) in enumerate(v)
        @inbounds V₁[u] = i
        @inbounds V₂[u] = i
    end

    blacklist = [i for i in v]

    for u in v
        for n in neighbors(G, u)
            H, V = if nv(G₂) < nv(G₁) G₂, V₂ else G₁, V₁ end

            if n ∉ blacklist
                push!(blacklist, n)

                _add_edge!(H, V, n, u)
                _add_cluster!(H, V, G, n, blacklist)
            elseif n ∈ v
                _add_edge!(H, V, n, u)
            end
        end
    end

    G₁, G₂
end

function separate(G::SimpleGraph{T}, v::T) where T <: Integer
    G₁  = SimpleGraph{T}(1)
    G₂  = SimpleGraph{T}(1)
    V₁  = zeros(T, nv(G))
    @inbounds V₁[v] = 1
    V₂ = copy(V₁)
    blacklist = [v]

    for n in neighbors(G, v)
        H, V = if nv(G₂) < nv(G₁) G₂, V₂ else G₁, V₁ end

        if n ∉ blacklist
            push!(blacklist, n)

            _add_edge!(H, V, n, v)
            _add_cluster!(H, V, G, n, blacklist)
        end
    end

    G₁, G₂
end

# Helper functions for performing partitions and separations
function _add_cluster!(H::SimpleGraph{T}, vmapping::Array{T, 1}, G::SimpleGraph{T},
                      u::T, blacklist::Array{T, 1}) where T <: Integer
    if degree(G, u) == 1
        return
    end

    for n in neighbors(G, u)
        if n == u
            continue
        end

        _add_edge!(H, vmapping, u, n)

        if n ∉ blacklist
            push!(blacklist, n)
            _add_cluster!(H, vmapping, G, n, blacklist)
        end
    end
end

function _add_edge!(G::SimpleGraph{T}, vmapping::Array{T, 1}, u::T, v::T) where T <: Integer
    if vmapping[u] == 0
        add_vertex!(G)
        vmapping[u] = nv(G)
    end

    if vmapping[v] == 0
        add_vertex!(G)
        vmapping[v] = nv(G)
    end

    add_edge!(G, vmapping[u], vmapping[v])
end

"""
		partition(G[, V])

Partitions `G` into two components, where `V` is the vertex set of a single
partition. If `V` isn't passed, returns the partiton given by the Fiedler
eigenvector. Returns the two graphs of the partition and the vertices that
each graph contains.
"""
function partition(G::SimpleGraph{T}, V::Array{T, 1}) where T <: Integer
    # Verify that the preconditions are met
    allunique(V) || throw(ArgumentError("Vertices in the partition must be unique"))
    # Create the graphs in the partition
    G₁ = SimpleGraph(length(V))
    G₂ = SimpleGraph(nv(G) - nv(G₁))
    # Vertex set of G₁, G₂
    V₁ = Set(V)
    V₂ = setdiff(vertices(G), V₁)

    # Create a mapping from V -> vertices(G₁) ∪ vertices(G₂)
    mapping = Dict{T, T}()
    for ((i, u), (j, v)) in zip(enumerate(V₁), enumerate(V₂))
        mapping[u] = T(i)
        mapping[v] = T(j)
    end

    # Add the proper edges to G₁ and G₂
    for (v₁, v₂) in zip(V₁, V₂)
        for (n₁, n₂) in zip(outneighbors(G, v₁), outneighbors(G, v₂))
            if n₁ in V₁
                add_edge!(G₁, mapping[v₁], mapping[n₁])
            end

            if n₂ in V₂
                add_edge!(G₂, mapping[v₂], mapping[n₂])
            end
        end
    end

    G₁, G₂
end

function partition(G::SimpleGraph)
    # Get the fiedler eigenvector
    ν = eigvecs(Symmetric(Matrix(laplacian_matrix(G))))[:,2]
    # Find the vertices of one of the components
    V = filter(i -> ν[i] > 0, vertices(G))

    partition(G, V)
end

"""
    associate!(G, u, v)

Vertex association merges `u` and `v` into one vertex by adding edges of
the form ``(u, j)``, for each ``j \\sim v`` and ``(j, u)`` for each
``v \\sim j``. The vertex `v` is then removed.
"""
function associate!(G::AbstractGraph{T}, u::T, v::T) where T <: Integer
    if u == v
        return false
    end

    for n in inneighbors(G, u)
        add_edge!(G, n, v)
        rem_edge!(G, n, u)
    end

    for n in outneighbors(G, u)
        add_edge!(G, v, n)
        rem_edge!(G, u, n)
    end

    rem_vertex!(G, u)
end

"""
    associate(G, u, v)

Returns a copy of `G` with `u` and `v` as an associated vertex.
"""
function associate(G::AbstractGraph{T}, u::T, v::T) where T <: Integer
    res = copy(G)
    associate!(res, u, v)

    res
end

"""
    attach!(G, u, H, v)

Attachs `H` to `G` by associating `v` with `u`.
"""
function attach!(G::U, u::T, H::U, v::T) where U <: AbstractGraph{T} where T <: Integer
    ~has_vertex(G, u) && throw(DomainError("u must be a vertex in G"))
    ~has_vertex(H, v) && throw(DomainError("v must be a vertex in H"))

    N = nv(G)
    M = nv(H)
    add_vertices!(G, M - 1)
    vmapping = x -> if x < v N+x elseif x > v N+x-1 else u end

    for e in edges(H)
        add_edge!(G, vmapping(e.src), vmapping(e.dst))
    end
end

"""
    attach(G, u, H, v)

Returns a graph where `H` is attached to `G` by associating `u` with `v`.
"""
function attach(G::U, u::T, H::U, v::T) where U <: AbstractGraph{T} where T <: Integer
    res = copy(G)
    attach!(res, u, H, v)

    res
end
