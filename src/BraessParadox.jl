module BraessParadox

import LinearAlgebra: # Types
                      Symmetric, Diagonal,
                      # Operators
                      eigvecs

import LightGraphs: # Types
                    AbstractGraph, AbstractEdge, SimpleGraph,
                    # Iterators
                    vertices, edges, neighbors, inneighbors, outneighbors,
                    # Invariants
                    nv, degree, is_connected, is_directed, has_vertex,
                    # Matrices
                    laplacian_matrix, adjacency_matrix,
                    # Modifiers
                    add_edge!, rem_edge!, add_vertex!, add_vertices!,
                    rem_vertex!

import ResistanceDistance: resistance_distance, rdist, kemenys_constant,
                           kconst, spanning_trees, trees, spanning_forests,
                           forests

export # Generators
       rand_tree, tree, barbell, conjoined_cycle,
       # Resistance and Braess Paradox functions
       resistance_distance, rdist, kemenys_constant, kconst,
       spanning_trees, trees, spanning_forests, forests,
       is_braess, braess_edges, braess_closure, braess_closure!,
       # Graph operators
       separate, partition, associate, associate!, attach, attach!

include("Generators/Generators.jl")
using .Generators

include("operators.jl")

"""
    is_braess(G, e; atol)

Returns true if `e` is a braess edge of `G`.

# Optional Keyword Arguments
- `atol` - the floating-point tolerance for determining if `e` satisfies
  Braess' paradox.

# Examples
```jldoctest
julia> G = star_graph(27)
{27, 26} undirected simple Int64 graph

julia> is_braess(G, 4, 17)
true
```
"""
function is_braess(G::AbstractGraph{T}, u::T, v::T; atol=1e-8) where T <: Integer
    !is_connected(G) && return false

    Ḡ = copy(G)
    !add_edge!(Ḡ, u, v) && return false

    (kemenys_constant(Ḡ) - kemenys_constant(G)) > atol
end

is_braess(G::AbstractGraph{T}, e::AbstractEdge{T}; keyargs...) where T <: Integer=
    is_braess(G, e.src, e.dst; keyargs...)

is_braess(G, u, v; keyargs...)=is_braess(G, promote(u, v)...; keyargs...)

"""
    is_braess(G, ℒ; atol)

Returns `true` if `ℒ` is a Braess set of `G`.
"""
function is_braess(G::AbstractGraph{T}, ℒ::Array{Tuple{T, T}}; atol=1e-8) where T <: Integer
    Ḡ = copy(G)

    for e in ℒ
        add_edge!(Ḡ, e)
    end

    (kemenys_constant(Ḡ) - kemenys_constant(G)) > atol
end

is_braess(G::AbstractGraph{T}, ℒ::Set{AbstractEdge{T}}; keyargs...) where T <: Integer=
    is_braess(G, [(e.src, e.dst) for e in ℒ]; keyargs...)

is_braess(G::AbstractGraph{T}, e::AbstractEdge{T}...; keyargs...) where T <: Integer=
    is_braess(G, [(edge.src, edge.dst) for edge in e]; keyargs...)

"""
    braess_edges(G; atol)

Finds all braess edges of `G` and returns them as a list of tuples.

# Optional Keyword Arguments
- `atol` - the floating point tolerance for determing if an edge satisfies
  Braess' paradox
"""
function braess_edges(G::AbstractGraph{T}; atol=1e-4) where T <: Integer
    N = nv(G)
    res = Array{Tuple{T, T}, 1}()

    if is_directed(G)
        for i=1:N
            for j=1:N
                if i != j && is_braess(G, i, j)
                    push!(res, (i, j))
                end
            end
        end
    else
        for i=1:N
            for j=i:N
                if i != j && is_braess(G, i, j)
                    push!(res, (i, j))
                end
            end
        end
    end

    res
end

"""
    braess_closure!(G)

Adds all possible edges to `G` until the resulting graph has no Braess
edges.
"""
function braess_closure!(G::AbstractGraph)
    n = nv(G)
    braess = true

    while braess

        braess = false

        for i=1:n
            for j=(i+1):n
                if is_braess(G, i, j)
                    add_edge!(G, i, j)
                    braess = true
                end
            end
        end
    end
end


"""
    braess_closure(G)

Returns the braess closure of `G`.
"""
function braess_closure(G::AbstractGraph)
    res = copy(G)

    braess_closure!(res)

    res
end

end # module
