export rand_tree, tree

"""
    rand_tree(n[, seed])

Returns a tree generated from a random Prüfer sequence of length `n`.

### Optional Keyword Arguments
`seed` - if defined, sets the seed for the random number generator

### Examples
```jldoctest
julia> using BraessParadox, LightGraphs;

julia> G = rand_tree(6; seed=703)
{6, 6} undirected simple Int64 graph

julia> adjacency_matrix(G)
6×6 SparseArrays.SparseMatrixCSC{Int64,Int64} with 11 stored entries:
  [2, 1]  =  1
  [5, 1]  =  1
  [1, 2]  =  1
  [3, 3]  =  2
  [4, 3]  =  1
  [5, 3]  =  1
  [6, 3]  =  1
  [3, 4]  =  1
  [1, 5]  =  1
  [3, 5]  =  1
  [3, 6]  =  1
```
"""
function rand_tree(n::T; seed::Union{Int, Nothing}=nothing)::SimpleGraph{T} where T <: Integer
    if typeof(seed) != Nothing
        seed!(seed)
    end

    seq = rand(1:n, n - 2)
    tree(seq)
end

"""
    tree(s)

Generates a tree from from the Prüfer sequence `s`.

### Examples
```jldoctest
julia> using BraessParadox, LightGraphs;

julia> G = tree([1, 1, 2])
{5, 5} undirected simple Int64 graph

julia> adjacency_matrix(G)
5×5 SparseArrays.SparseMatrixCSC{Int64,Int64} with 9 stored entries:
  [2, 1]  =  1
  [3, 1]  =  1
  [4, 1]  =  1
  [1, 2]  =  1
  [2, 2]  =  2
  [5, 2]  =  1
  [1, 3]  =  1
  [1, 4]  =  1
  [2, 5]  =  1
```
"""
function tree(s::Array{T, 1})::SimpleGraph{T} where T <: Integer
    n   = size(s, 1) + 2
    D   = ones(Int, n)
    adj = zeros(Int, n ,n)

    for u in s
        D[u] += 1
    end

    for u in s
        for v in 1:n
            if D[v] == 1
                D[v] -= 1
                D[u] -= 1
                adj[u, v] = adj[v, u] = 1
                break
            end
        end
    end

    for u in 1:n
        if D[u] == 1
            for v in (u+1):n
                if D[v] == 1
                    D[u] -= 1
                    D[v] -= 1
                    adj[u, v] = adj[v, u] = 1
                end
            end
        end
    end

    SimpleGraph(adj)
end

tree(s::Int...)=tree([i for i in s])
