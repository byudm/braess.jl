export conjoined_cycle

"""
    conjoined_cycle(n, m)

Returns the conjoined cycle created by attaching a copy of ``C_m`` to ``C_n``.

### Examples
```jldoctest
julia> using BraessParadox;

julia> G = conjoined_cycle(5, 7)
{11, 12} undirected simple Int64 graph
```
"""
function conjoined_cycle(n::T, m::T) where T <: Integer
    G = SimpleGraph{T}(n + m - 1)

    for i=1:(n-1)
        add_edge!(G, i, i+1)
    end

    add_edge!(G, 1, n)

    for i=n:(n+m-2)
        add_edge!(G, i, i+1)
    end

    add_edge!(G, n, n+m-1)

    G
end

conjoined_cycle(n, m)=conjoined_cycle(promote(n, m)...)
