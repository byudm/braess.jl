export barbell

"""
    barbell(m, n) → G

Returns the barbell graph with a path with `m` vertices connecting two complete
graphs on `n` vertices.

# Examples
```jldoctest
julia> using BraessParadox;

julia> G = barbell(5, 3)
{12, 23} undirected simple Int64 graph
```
"""
function barbell(m::T, n::T)::SimpleGraph{T} where T <: Integer
    nv = 2n + m
    ne = n*(n-1) + m+1
    fadjlist = Vector{Vector{T}}(undef, nv)

    # Make the complete graphs
    @inbounds @simd for u=1:(n-1)
        adj = fill(T(0), n-1)
        adj[1:(u-1)] = 1:(u-1)
        adj[u:(n-1)] = (u+1):n
        fadjlist[u] = adj
        fadjlist[nv-u+1] = (nv + 1) .- reverse(adj)
    end

    # Create the connections between the path and barbells
    adj = fill(T(0), n)
    adj[1:(n-1)] = 1:(n-1)
    adj[n] = n + 1
    fadjlist[n] = adj
    fadjlist[nv-n+1] = (nv+1) .- reverse(adj)

    # Create the path
    for u=(n+1):(nv-n)
        fadjlist[u] = T[u-1, u+1]
    end

    SimpleGraph{T}(ne, fadjlist)
end

barbell(m, n) = barbell(promote(m, n)...)

"""
    barbell(k, m, n) → G

Returns a barbell with `k` paths of length `m` connecting two complete
graphs on `n` vertices.

# Examples
```jldoctest
julia> using BraessParadox;

julia> G = barbell(5, 3, 4);
```
"""
function barbell(k::T, m::T, n::T)::SimpleGraph{T} where T <: Integer
    SimpleGraph{T}()
end

barbell(k, m, n) = barbell(promote(k, m, n)...)
